import re
import os, os.path

def getTxt(record):
    path = "concept_assertion_relation_training_data/beth/txt/record-" + record + ".txt"
    return open(path, "r").readlines()


def getProblems(concepts):
    concepts = concepts.split("\n")
    problems = []
    for concept in concepts:
        if(re.search("problem", concept) != None):
            problems.append(concept)

    return problems

def getConcepts(record):
    path = "concept_assertion_relation_training_data/beth/concept/" + record
    return open(path, "r").read()

problem_list = []

i = 0
for name in os.listdir('./concept_assertion_relation_training_data/beth/concept/'):
    for problem in getProblems(getConcepts(name)):
        i += 1
        # if(i > 20000):
            # break
        problem_list.append( re.search("c=\"([a-z 0-9\'\.\+\-\,\%\&\>\<\=\(\)\/]+)\"", problem).group().split("=")[1].strip('"'))
        # print("\n" + problem)
        # print(re.search("c=\"([a-z 0-9\'\.\+\-\,\%\&\>\<\=\(\)\/]+)\"", problem).group())


# for p in problem_list:
#     print(p)

print(len(problem_list))

problem_set = set(problem_list)
print(len(problem_set))

# YML file format
# Comments: Chest
# Direction: ''
# Lex: pulmonary embolism
# Regex: pulmonary\s(artery )?(embol[a-z]+)|\bpe\b|pulmonary thromboembolic disease
# Type: PULMONARY_EMBOLISM
# ---

i = 0
with open('kb.yml', 'a') as f:
    for p in problem_set:
        i += 1
        # if (i > 2):
        #     break
        regEx = re.escape(p)
        f.write('Comments: \'\'\n')
        f.write('Direction: \'\'\n')
        f.write('Lex: ' + p + '\n')
        f.write('Regex: ' + regEx + '\n')
        f.write('Type: NA\n')
        f.write('--- \n')

print(i)