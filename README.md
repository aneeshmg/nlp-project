Instructions on how to run AssertionClassifier
==============================================

* Download the i2b2 dataset and place it in the same folder as AssertionClassifier.java and ConText.java (Folder named "concept_assertion_relation_training_data")
* Delete record-58.{txt,ast} from the respective folders inside `beck` (As the assertion files are empty and cannot use it to validate our results.
* ner-data contains the data output from the NER module, place it in the same directory.
* compile the program by running ``javac ConText.java AssertionClassfier.java``
* Run the program using ``java AssertionClassifier``
* Results are printed on the console.
