import itemData
import pyConTextNLP.pyConTextGraph as pyConText
import networkx as nx

def getTxt(record):
    path = "concept_assertion_relation_training_data/beth/txt/record-" + record + ".txt"
    return open(path, "r").readlines()

reports = [
    """IMPRESSION: Evaluation limited by lack of IV contrast; however, no evidence of
      bowel obstruction or mass identified within the abdomen or pelvis. Non-specific interstitial opacities and bronchiectasis seen at the right
     base, suggestive of post-inflammatory changes.""",
    """IMPRESSION: Evidence of early pulmonary vascular congestion and interstitial edema. Probable scarring at the medial aspect of the right lung base, with no
     definite consolidation."""
    ,
    """IMPRESSION:
     
     1.  2.0 cm cyst of the right renal lower pole.  Otherwise, normal appearance
     of the right kidney with patent vasculature and no sonographic evidence of
     renal artery stenosis.
     2.  Surgically absent left kidney.""",
    """IMPRESSION:  No pneumothorax.""",
    """IMPRESSION: No definite pneumothorax"""
    """IMPRESSION:  New opacity at the left lower lobe consistent with pneumonia."""
]

i2b2_report = getTxt("13")

modifiers = itemData.get_items(
    'file:///home/aneeshmg/dev/personal/nlp/project/lexical_kb_05042016.yml')
    # "https://raw.githubusercontent.com/chapmanbe/pyConTextNLP/master/KB/lexical_kb_05042016.yml")
targets = itemData.get_items(
    "file:///home/aneeshmg/dev/personal/nlp/project/utah_crit.yml")
    # "https://raw.githubusercontent.com/chapmanbe/pyConTextNLP/master/KB/utah_crit.yml")

i2b2_targets = itemData.get_items("file:///home/aneeshmg/dev/personal/nlp/project/kb.yml")
targets = i2b2_targets

def markup_sentence(s, modifiers, targets, prune_inactive=True):
    """
    """
    markup = pyConText.ConTextMarkup()
    markup.setRawText(s)
    markup.cleanText()
    markup.markItems(modifiers, mode="modifier")
    markup.markItems(targets, mode="target")
    markup.pruneMarks()
    markup.dropMarks('Exclusion')
    # apply modifiers to any targets within the modifiers scope
    markup.applyModifiers()
    markup.pruneSelfModifyingRelationships()
    if prune_inactive:
        markup.dropInactiveModifiers()
    return markup

# print (i2b2_report[114])

markup = pyConText.ConTextMarkup()

# print(isinstance(markup,nx.DiGraph))

markup.setRawText(i2b2_report[36].lower()) #WIP
# print(markup)
# print(len(markup.getRawText()))

markup.cleanText()
print(markup)
# print(len(markup.getText()))


markup.markItems(modifiers, mode="modifier")
print(markup.nodes(data=True))

print(type(list(markup.nodes())[0]))

markup.markItems(targets, mode="target")

for node in markup.nodes(data=True):
    print(node)

markup.pruneMarks()
for node in markup.nodes(data=True):
    print(node)

print(markup.edges())

markup.applyModifiers()
for edge in markup.edges():
    print(edge)