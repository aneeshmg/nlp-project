import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.Writer;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.*;  

class ProblemBeginIO {
    int begin = 0, end = 0, line = 0;
    ProblemBeginIO(int line, int begin, int end) {
        this.line = line;
        this.begin = begin;
        this.end = end;
    }
    void print() {
        System.out.println("Line: " + Integer.toString(line) + " Begin: " + Integer.toString(begin) + " End: " + Integer.toString(end));
    }
}
class FileStructure {
    String txtFile = "";
    String conFile = "";
    String astFile = "";
    FileStructure(String t, String c, String a) {
        this.txtFile = t;
        this.conFile = c;
        this.astFile = a;
    }
    void print() {
        System.out.println(this.txtFile + " " + this.conFile + " " + astFile);
    }
}

class AssertionClassifier {

    private static String readFile(String fileName) throws IOException {

            String line;
            StringBuilder sb;
            String fileContents = null;

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                sb = new StringBuilder();
                line = bufferedReader.readLine().toLowerCase();

                while(line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = bufferedReader.readLine();
                    if(line == null) break;
                    else line = line.toLowerCase();
                }
                fileContents = sb.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                line = null;
                sb = null;
            }
            if (fileContents.equals("")) throw new IOException();
            return fileContents;
        }

    public static void main(String[] args) {

        ConText conText = new ConText();
        ArrayList<ProblemBeginIO> problems;
        ArrayList<FileStructure> fs = new ArrayList<>();
        String actualAssertion = "";
        String predictedAssertion = "";
        int total = 0;
        int truePositive = 0;
        int trueNegative = 0;
        int falsePositive = 0;
        int falseNegative = 0;
        double precision = 0;
        double recall = 0;
        double f1 = 0;

        String astBaseB = "./concept_assertion_relation_training_data/beth/ast/";
        String astBaseP = "./concept_assertion_relation_training_data/partners/ast/";
        String txtBaseB = "./concept_assertion_relation_training_data/beth/txt/";
        String txtBaseP = "./concept_assertion_relation_training_data/partners/txt/";
        String conBaseB = "./ner-data/beth/";
        String conBaseP = "./ner-data/parners/";

        File txtBeth = new File("./concept_assertion_relation_training_data/beth/txt/");
        File txtPartners = new File("./concept_assertion_relation_training_data/partners/txt/");

        ArrayList<String> txtFilesBase = new ArrayList<String>(Arrays.asList(txtBeth.list()));
        ArrayList<String> tP = new ArrayList<String>(Arrays.asList(txtPartners.list()));

        for (String n : txtFilesBase) {
            n = n.substring(0, n.length() - 4);
            FileStructure f = new FileStructure(txtBaseB + n + ".txt", conBaseB + n + "-tag.tsv", astBaseB + n + ".ast");
            fs.add(f);
        }
        // for (String n : tP) {
        //     n = n.substring(0, n.length() - 4);
        //     FileStructure f = new FileStructure(txtBaseP + n + ".txt", conBaseP + n + "-tag.tsv", astBaseP + n + ".ast");
        //     fs.add(f);
        // }
        for (FileStructure f : fs) {

            String[] txtFile;
            try {
                txtFile = readFile(f.txtFile).split("\n");
            } catch (IOException e) {
                Log(e);
                continue;
            }
            String astRaw;
            // Log(f.astFile);
            try {
               astRaw = readFile(f.astFile);
            } catch (IOException e) {
                Log(e);
                continue;
            }
            problems = getProblems(f.conFile);
            String problemLine;
            String problem;
            for (ProblemBeginIO p : problems) {
                problemLine = txtFile[p.line];
                problem = "";
                for(int i = p.begin; i <= p.end; i++) {
                    if(i < problemLine.split(" ").length)
                        problem += problemLine.split(" ")[i] + " ";
                }

                ArrayList<String> processContext = new ArrayList<>();
                try {
                    processContext = conText.applyContext(problem, problemLine);
                } catch(Exception e) {
                    System.out.println(e);
                }
                if(processContext != null) {
                    // for(String s:processContext) {
                    //     System.out.println(s);
                    // }
                    switch(processContext.get(2)) {
                        case "Affirmed" :
                            predictedAssertion = "present";
                            break;
                        case "Negated" :
                            predictedAssertion = "absent";
                            break;
                        case "Possible" :
                            predictedAssertion = "hypothetical";
                            break;
                        default: predictedAssertion = "present";
                    }
                    if(processContext.get(4).equals("Other"))
                        predictedAssertion = "associated_with_someone_else";
                }
                actualAssertion = getAssertion(problem, astRaw);
                if(actualAssertion.equals("present")) {
                    if(predictedAssertion.equals(actualAssertion))
                        truePositive++;
                    else 
                        falseNegative++;
                } else if (actualAssertion.equals("absent")) {
                    if(predictedAssertion.equals(actualAssertion))
                        trueNegative++;
                    else
                        falsePositive++;
                } else total++; // Hypothetical scenario
                total += truePositive + trueNegative + falsePositive + falseNegative;
            }
        }

        precision = (double) truePositive / ((double) truePositive + (double) falsePositive);
        recall = (double) truePositive / ((double) truePositive + (double) falseNegative);
        f1 = 2 * (precision * recall) / (precision + recall);

        Log("Precision: " + precision);
        Log("Recall: " + recall);
        Log("F1: " + f1);
        
    }
    private static void Log(Object s) {
        System.out.println(s);
    }
    private static ArrayList<ProblemBeginIO> getProblems(String filePath) {

        String[] conFile;
        try {
           conFile = readFile(filePath).split("\n");
        } catch (IOException e) {
            Log(e);
            return null;
        }

        
        ArrayList<ProblemBeginIO> problems = new ArrayList<ProblemBeginIO>();
        int begin = 0, end = 0;
        for(int j = 0; j < conFile.length; j++) {
            String[] problemLine = conFile[j].split("\\t");
            for(int i = 0; i < problemLine.length; i++) {
                boolean beginFlag = false;
                if(problemLine[i].equals("problem-b")) {
                    beginFlag = true;
                    begin = i;
                    while(i < problemLine.length && !problemLine[i].equals("o")) {
                        i++;
                    }
                    i--;
                    end = i;
                    ProblemBeginIO p = new ProblemBeginIO(j, begin, end);
                    problems.add(p);
    
                }
            }
        }
        return problems;
    }
    private static String getAssertion(String problem, String file) {

        final Scanner scanner = new Scanner(file);
        Pattern assertionRegex = Pattern.compile("(present)|(absent)|(hypothetical)|(associated_with_someone_else)");
        Matcher assertionMatcher;
        String assertion = "hypothetical";
        String[] problemWords = problem.split(" ");
        boolean found = false;

        while (scanner.hasNextLine()) {
            final String lineFromFile = scanner.nextLine();

            for (String p : problemWords) {
                if(lineFromFile.indexOf(p) > -1) {
                    found = true;
                }
            }
            if(found == true) {
                assertionMatcher = assertionRegex.matcher(lineFromFile);
                if(assertionMatcher.find() == true) {
                    assertion = assertionMatcher.group().toString();
                    found = false;
                    break;
                }
            } else assertion = "hypothetical";

        }
        return assertion;
        
    }
}