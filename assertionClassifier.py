import re

def getConcepts(record):
    path = "concept_assertion_relation_training_data/beth/concept/record-" + record + ".con"
    return open(path, "r").read()


def getTxt(record):
    path = "concept_assertion_relation_training_data/beth/txt/record-" + record + ".txt"
    return open(path, "r").readlines()


def getProblems(concepts):
    concepts = concepts.split("\n")
    problems = []
    for concept in concepts:
        if(re.search("problem", concept) != None):
            problems.append(concept)

    return problems

i = 0
for p in getProblems(getConcepts("13")):
    i += 1
    lineNumbers = re.search("[0-9]+:[0-9]+ [0-9]+:[0-9]+", p).group().split(" ")
    startIndex = lineNumbers[0]
    wordStartIndex = int(startIndex.split(":")[1])
    endIndex = lineNumbers[1]
    wordEndIndex = int(endIndex.split(":")[1]) + 1
    # print(startIndex.split(":")[0])
    txt = getTxt("13")
    line = txt[int(startIndex.split(":")[0])-1]
    print (" ".join(line.split(" ")[wordStartIndex:wordEndIndex]))
    if(i > 5):
        break
